<?php

namespace MWS\Sample\Plugin;

use MWS\Sample\Model\SampleLogFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;

/**
 * Class SampleLog
 * @package MWS\Sample\Plugin
 */
class SampleLog
{
    /**
         * @var \MWS\Sample\Model\SampleLogFactory
    	     */
    protected $_sampleLogFactory;

    protected $_sampleLog;

    protected $_scopeConfig;


    /**
     * @param SampleLogFactory $sampleLogFactory
     */
    public function __construct(
        SampleLogFactory $sampleLogFactory,
        ScopeConfigInterface $scopeConfig
    )
    {
        $this->_sampleLogFactory = $sampleLogFactory;
        $this->_scopeConfig = $scopeConfig;
    }

    /**
     * @param \Magento\Framework\Model\AbstractModel $subject
     * @param $result
     * @return mixed
     * @throws \Exception
     */
    public function afterSave(
        \Magento\Framework\Model\AbstractModel $subject,
        $result
    ) {
        if ($this->getIsEnabled()){
            if($result->getState()) {
                $order_id = $result->getRealOrderId();
                $order_total_paid = $result->getTotalPaid();
                $order_total = $result->getGrandTotal();
                if ($order_total - $order_total_paid == 0){
                    $decimal_factor = $this->getDecimalFactor();
                    $model = $this->_sampleLogFactory->create();
                    $model->addData([
                        "order_id" => $order_id,
                        "paid_total_sum" => $decimal_factor*$order_total
                    ]);
                    $model->save();
                }

            }
        }
        return $result;
    }

    /**
     * @return bool
     */
    private function getIsEnabled(){
        return $this->_scopeConfig->getValue(
            "sample/enabled/enable",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        ) == 1;
    }

    /**
     * @return mixed
     */
    private function getDecimalFactor(){
        return $this->_scopeConfig->getValue(
            "sample/decimal_factor/decimal_factor",
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

}