<?php
namespace MWS\Sample\Setup;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(\Magento\Framework\Setup\SchemaSetupInterface $setup, \Magento\Framework\Setup\ModuleContextInterface $context)
    {
        /**
         * Create table 'mws_sample_log'
         */
        $installer = $setup;
        $installer->startSetup();
        if (!$installer->tableExists('mws_sample_log')) {
            $table = $installer->getConnection()
                ->newTable(
                    $installer->getTable('mws_sample_log')
                )
                ->addColumn(
                    'log_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'nullable' => false,
                        'primary'  => true,
                        'unsigned' => true,
                    ],
                    'Log ID'
                )
                ->addColumn(
                    'order_id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    255,
                    ['nullable' => false],
                    'Order ID'
                )
                ->addColumn(
                    'paid_total_sum',
                    \Magento\Framework\DB\Ddl\Table::TYPE_FLOAT,
                    255,
                    [],
                    'Paid Total Sum'
                )
                ->addColumn(
                    'created_at',
                    \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT],
                    'Created At'
                );
            $installer->getConnection()->createTable($table);
        }
        $installer->endSetup();
    }
}