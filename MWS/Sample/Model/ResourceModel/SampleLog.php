<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 5/4/18
 * Time: 11:59 AM
 */

namespace MWS\Sample\Model\ResourceModel;


class SampleLog extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init('mws_sample_log', 'log_id');
    }
}