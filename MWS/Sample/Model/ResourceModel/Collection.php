<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 5/4/18
 * Time: 12:01 PM
 */

namespace MWS\Sample\Model\ResourceModel\SampleLog;


class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection{
    public function _construct(){
        $this->_init("MWS\Sample\Model\SampleLog","MWS\Sample\Model\ResourceModel\SampleLog");
    }
}