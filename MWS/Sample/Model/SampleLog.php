<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 5/4/18
 * Time: 11:56 AM
 */

namespace MWS\Sample\Model;

class SampleLog extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'mws_sample_log';

    protected function _construct()
    {
        $this->_init('MWS\Sample\Model\ResourceModel\SampleLog');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getDefaultValues()
    {
        $values = [];

        return $values;
    }
}
